package com.maks1m.tictactoe.board;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Point;

public abstract class AbstractCell extends Point {
    public AbstractCell(int x, int y) {
        super(x, y);
    }

    abstract public void draw(Canvas c, Resources res, int x, int y, int w, int h, int delta);
}
