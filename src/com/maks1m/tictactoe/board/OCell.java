package com.maks1m.tictactoe.board;

import android.content.res.Resources;
import android.graphics.*;
import com.maks1m.tictactoe.R;

public class OCell extends AbstractCell {
    public OCell(int x, int y) {
        super(x, y);
    }

    @Override
    public void draw(Canvas c, Resources res, int x, int y, int w, int h, int delta) {
        Bitmap im = BitmapFactory.decodeResource(res, R.drawable.o_cell);
        c.drawBitmap(im, null, new Rect(x * w, y * h + delta, (x * w) + w, (y * h) + h + delta), new Paint());
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof OCell) ? true : false;
    }

    @Override
    public String toString() {
        return "O cell";
    }
}
