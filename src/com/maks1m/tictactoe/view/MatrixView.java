package com.maks1m.tictactoe.view;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.maks1m.tictactoe.R;
import com.maks1m.tictactoe.activity.BoardActivity;
import com.maks1m.tictactoe.board.AbstractCell;
import com.maks1m.tictactoe.board.EmptyCell;
import com.maks1m.tictactoe.board.OCell;
import com.maks1m.tictactoe.board.XCell;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MatrixView extends View {
    private AbstractCell[][] gameMatrix = null;
    private Paint paint, paintBlur, paintWin, paintWinBlur;
    private boolean userMove;

    private final int boardSize = 3;
    private final int cellWinCount = 3;
    private final long gameLoopDelay = 300;
    private boolean doInvalidate = true;

    private long lastMove = 0;

    private int userScore = 0;
    private int droidScore = 0;

    private TextView userScoreValue;
    private TextView droidScoreValue;

    private char userShape;

    private RefreshHandler handler = new RefreshHandler();
    private WinLine winLine = null;

    class RefreshHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    if (doInvalidate) {
                        MatrixView.this.invalidate();
                    }
                    MatrixView.this.update();
                    break;
                case 1:
                    Toast.makeText(getContext(), "You win!", Toast.LENGTH_LONG).show();
                    break;
                case 2:
                    Toast.makeText(getContext(), "Droid win!", Toast.LENGTH_LONG).show();
                    break;
                case 3:
                    Toast.makeText(getContext(), "Draw!", Toast.LENGTH_LONG).show();
                    break;
                case 10:
                    Toast.makeText(getContext(), "You move first", Toast.LENGTH_LONG).show();
                    break;
                case 11:
                    Toast.makeText(getContext(), "Droid moves first!", Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }

            super.handleMessage(msg);
        }

        public void sleep(long delayMillis) {
            this.removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMillis);
        }
    }

    //main game loop
    private void update() {
        long now = System.currentTimeMillis();

        if (now - lastMove >= gameLoopDelay) {
            if (!checkValidate()) {
                if (isFull()) {
                    zeroGame();
                } else {
                    droidMove();
                }
            }

            lastMove = now;
        }

        handler.sleep(gameLoopDelay);
    }

    public MatrixView(Context context) {
        super(context);

        paint = new Paint();
        paint.setARGB(255, 0, 0, 0);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setDither(true);
        paint.setColor(Color.argb(248, 255, 255, 255));

        //this one will paint blur line
        paintBlur = new Paint();
        paintBlur.set(paint);
        paintBlur.setColor(Color.argb(235, 74, 138, 255));
        paintBlur.setStrokeWidth(6);
        paintBlur.setMaskFilter(new BlurMaskFilter(7, BlurMaskFilter.Blur.NORMAL));

        paintWin = new Paint();
        paintWin.setARGB(255, 0, 0, 0);
        paintWin.setAntiAlias(true);
        paintWin.setStyle(Paint.Style.STROKE);
        paintWin.setStrokeWidth(22);
        paintWin.setStrokeJoin(Paint.Join.ROUND);
        paintWin.setStrokeCap(Paint.Cap.ROUND);
        paintWin.setDither(true);
        paintWin.setColor(Color.argb(248, 255, 255, 255));

        paintWinBlur = new Paint();
        paintWinBlur.set(paint);
        paintWinBlur.setColor(Color.argb(208, 74, 193, 91));
        paintWinBlur.setStrokeWidth(26);
        paintWinBlur.setMaskFilter(new BlurMaskFilter(7, BlurMaskFilter.Blur.NORMAL));

        gameMatrix = new AbstractCell[boardSize][boardSize];

        int xss = getWidth() / boardSize;
        int yss = getWidth() / boardSize;

        for (int z = 0; z < boardSize; z++) {
            for (int i = 0; i < boardSize; i++) {
                gameMatrix[z][i] = new EmptyCell(xss * i, z * yss);
            }
        }

        userMove = ((int) (Math.random() * 10) % 2 == 1) ? true : false;
        userShape = BoardActivity.userShape.toCharArray()[0];

        handler.sendMessage(Message.obtain(handler, userMove ? 10 : 11));

        update();
    }

    public MatrixView(Context context, AttributeSet attrs) {
        this(context);
    }

    public MatrixView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int step = getWidth() / boardSize;
        int delta = (getHeight() - getWidth()) / 2;

        for (int row = 0; row < gameMatrix.length; row++) {
            for (int col = 0; col < gameMatrix[0].length; col++) {
                gameMatrix[row][col].draw(canvas, getResources(), col, row, step, step, delta);
            }
        }

        for (int i = 1; i < boardSize; i++) {
            //vertical
            canvas.drawLine(step * i, delta, step * i, getWidth() + delta, paint);
            canvas.drawLine(step * i, delta, step * i, getWidth() + delta, paintBlur);

            //horizontal
            canvas.drawLine(0, step * i + delta, this.getWidth(), step * i + delta, paint);
            canvas.drawLine(0, step * i + delta, this.getWidth(), step * i + delta, paintBlur);
        }

        if (winLine != null) {
            canvas.drawLine(winLine.start.x, winLine.start.y + delta, winLine.end.x, winLine.end.y + delta, paintWin);
            canvas.drawLine(winLine.start.x, winLine.start.y + delta, winLine.end.x, winLine.end.y + delta, paintWinBlur);
            winLine = null;
            zeroGame();
            doInvalidate = true;
        } else {
            doInvalidate = false;
        }

        super.onDraw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (userMove) {
            int delta = (getHeight() - getWidth()) / 2;
            int step = getWidth() / boardSize;

            if (event.getY() >= delta && event.getY() <= getHeight() - delta) {
                int x = (int) (event.getX() / step);
                int y = (int) ((event.getY() - delta) / step);
                drawImageOnTouchEvent(x, y);
            }
        }

        return super.onTouchEvent(event);
    }

    private void drawImageOnTouchEvent(int col, int row) {
        if (!(gameMatrix[row][col] instanceof EmptyCell)) {
            return;
        }

        AbstractCell cel = null;

        if (userMove) {
            if (userShape == 'x') {
                cel = new XCell(gameMatrix[row][col].x, gameMatrix[row][col].y);
            } else {
                cel = new OCell(gameMatrix[row][col].x, gameMatrix[row][col].y);
            }
        } else {
            if (userShape == 'x') {
                cel = new OCell(gameMatrix[row][col].x, gameMatrix[row][col].y);
            } else {
                cel = new XCell(gameMatrix[row][col].x, gameMatrix[row][col].y);
            }
        }

        gameMatrix[row][col] = cel;

        userMove = false;
        doInvalidate = true;
        handler.sendMessage(Message.obtain(handler, 0));
    }

    private void droidMove() {

        if (userMove) {
            return;
        }

        if (isFull()) {
            return;
        }

        if (doInvalidate) {
            return;
        }

        int col;
        int row;

        Class droidShape;

        if (userShape == 'x') {
            droidShape = OCell.class;
        } else {
            droidShape = XCell.class;
        }

        /* random algorithm
            find all empty cell
            then choose one with randomizer
         */

        List<int[]> combinations = new ArrayList<>();

        for (int y = 0; y < boardSize; y++) {
            for (int x = 0; x < boardSize; x++) {
                if (gameMatrix[y][x] instanceof EmptyCell) {
                    combinations.add(new int[]{y, x});
                }
            }
        }

        int randomNum = new Random().nextInt((combinations.size()));
        row = combinations.get(randomNum)[0];
        col = combinations.get(randomNum)[1];

        AbstractCell cel = null;

        if (userShape == 'x') {
            cel = new OCell(gameMatrix[row][col].x, gameMatrix[row][col].y);
        } else {
            cel = new XCell(gameMatrix[row][col].x, gameMatrix[row][col].y);
        }

        gameMatrix[row][col] = cel;

        userMove = true;
        doInvalidate = true;
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        if (visibility == VISIBLE) {
            ((TextView) getRootView().findViewById(R.id.userScoreValue)).setText("0");
            ((TextView) getRootView().findViewById(R.id.droidScoreValue)).setText("0");

            ImageButton imgBtnUser = (ImageButton) getRootView().findViewById(R.id.imgBtnUserShape);
            ImageButton imgBtnDroid = (ImageButton) getRootView().findViewById(R.id.imgBtnDroidShape);

            if (userShape == 'x') {
                imgBtnUser.setBackgroundResource(R.drawable.x_cell);
                imgBtnDroid.setBackgroundResource(R.drawable.o_cell);
            } else {
                imgBtnUser.setBackgroundResource(R.drawable.o_cell);
                imgBtnDroid.setBackgroundResource(R.drawable.x_cell);
            }

            if (userScoreValue == null) {
                userScoreValue = (TextView) getRootView().findViewById(R.id.userScoreValue);
            }

            if (droidScoreValue == null) {
                droidScoreValue = (TextView) getRootView().findViewById(R.id.droidScoreValue);
            }
        }
        super.onVisibilityChanged(changedView, visibility);
    }

    private boolean checkValidate() {
        if (doInvalidate) {
            return false;
        }

        Class winCellType = validateGame();

        if (winCellType != null) {
            if ((winCellType == XCell.class && userShape == 'x')
                    || (winCellType == OCell.class && userShape == 'o')) {
                userScoreValue.setText(String.valueOf(++userScore));
            } else {
                droidScoreValue.setText(String.valueOf(++droidScore));
            }

            doInvalidate = true;
            return true;
        }

        return false;
    }

    private Class validateGame() {
        int counter = 0;
        AbstractCell previous = null;
        int indent = (int) (this.getWidth() * 0.05);

        //horizontal line
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (gameMatrix[i][j] instanceof EmptyCell) {
                    break;
                }

                if (previous == null) {
                    previous = gameMatrix[i][j];
                    counter++;
                    continue;
                } else if (!gameMatrix[i][j].equals(previous)) {
                    break;
                } else {
                    counter++;
                }
            }

            if (counter >= cellWinCount) {
                int y = (this.getWidth() / boardSize) * i + (this.getWidth() / boardSize) / 2;

                winLine = new WinLine(new Dot(indent, y)
                        , new Dot(this.getWidth() - indent, y));

                return previous.getClass();
            }

            previous = null;
            counter = 0;
        }

        //vertical line
        for (int j = 0; j < boardSize; j++) {
            for (int i = 0; i < boardSize; i++) {
                if (gameMatrix[i][j] instanceof EmptyCell) {
                    break;
                }

                if (previous == null) {
                    previous = gameMatrix[i][j];
                    counter++;
                    continue;
                } else if (!gameMatrix[i][j].equals(previous)) {
                    break;
                } else {
                    counter++;
                }
            }

            if (counter >= cellWinCount) {
                int x = (this.getWidth() / boardSize) * j + (this.getWidth() / boardSize) / 2;

                winLine = new WinLine(new Dot(x, indent)
                        , new Dot(x, this.getWidth() - indent));

                return previous.getClass();
            }

            previous = null;
            counter = 0;
        }

        if (!(gameMatrix[0][0] instanceof EmptyCell)
                && gameMatrix[0][0].equals(gameMatrix[1][1])
                && gameMatrix[0][0].equals(gameMatrix[2][2])) {

            winLine = new WinLine(new Dot(indent, indent)
                    , new Dot(this.getWidth() - indent, this.getWidth() - indent));

            return gameMatrix[1][1].getClass();
        }

        if (!(gameMatrix[2][0] instanceof EmptyCell)
                && gameMatrix[2][0].equals(gameMatrix[1][1])
                && gameMatrix[2][0].equals(gameMatrix[0][2])) {

            winLine = new WinLine(new Dot(indent, this.getWidth() - indent)
                    , new Dot(this.getWidth() - indent, indent));

            return gameMatrix[1][1].getClass();
        }

        return null;
    }

    private boolean isFull() {
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (gameMatrix[i][j] instanceof EmptyCell) {
                    return false;
                }
            }
        }
        return true;
    }

    private void zeroGame() {
        int xss = this.getWidth() / boardSize;
        int yss = this.getWidth() / boardSize;

        for (int z = 0; z < boardSize; z++) {
            for (int i = 0; i < boardSize; i++) {
                gameMatrix[z][i] = new EmptyCell(xss * i, z * yss);
            }
        }

        doInvalidate = true;
    }

    class Dot {
        int x, y;

        public Dot(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    class WinLine {
        Dot start;
        Dot end;

        public WinLine(Dot end, Dot start) {
            this.end = end;
            this.start = start;
        }
    }
}
