package com.maks1m.tictactoe.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.maks1m.tictactoe.R;

public class TicTacToeActivity extends Activity {
    public final static String USER_SHAPE = "com.maks1m.tictactoe.USER_SHAPE";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public void choiceBtnPressed(View view) {
        Intent intent = new Intent(this, BoardActivity.class);
        intent.putExtra(USER_SHAPE, (view.getId() == R.id.choiceX) ? "x" : "o");
        startActivity(intent);
    }
}
