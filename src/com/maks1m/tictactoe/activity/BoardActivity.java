package com.maks1m.tictactoe.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.maks1m.tictactoe.R;
import com.maks1m.tictactoe.view.MatrixView;

public class BoardActivity extends Activity {
    private MatrixView matrixView;

    /* i tried to use (MatrixView) findViewById(R.id.MatrixGrid)
       and then use some setter but i got always null :(
      */
    public static String userShape;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        userShape = intent.getStringExtra(TicTacToeActivity.USER_SHAPE);

        setContentView(R.layout.game_field);
    }
}